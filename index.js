const http = require('http');
const url = require('url');
const {
    parse
} = require('querystring');
const fs = require('fs');

var listItem = [];
var currentUrl;

const server = http.createServer((req, res) => {


    currentUrl = url.parse(req.url, true);

    // Router - odbieranie wiadomości POST
    if (req.method == 'POST' && currentUrl.pathname == '/addpost') {
        console.log('Odebrałem żądanie POST');

        let body = [];
        req.on('data', (chunk) => {
            body.push(chunk);
        }).on('end', () => {
            listItem.push(parse(Buffer.concat(body).toString()));
        })

        res.writeHead(302, {
            'Location': '/'
        })
        res.end();
    }

    if (req.method == 'GET' && currentUrl.pathname == '/') {
        console.log('Odebrałem żądanie GET jestem na stronie głównej');

        let body = "";

        let pageHeader;
        let pageFooter;
        let formElement;

        if (fs.existsSync('partials/layout/Header.html')) {
            pageHeader = fs.readFileSync('partials/layout/Header.html', 'utf-8');
        }

        if (fs.existsSync('partials/layout/Footer.html')) {
            pageFooter = fs.readFileSync('partials/layout/Footer.html', 'utf-8');
        }

        if (fs.existsSync('partials/FormElement.html')) {
            formElement = fs.readFileSync('partials/FormElement.html', 'utf-8');
        }

        body += pageHeader;

        if (listItem.length > 0) {
            for (let i = 0; i < listItem.length; i++) {

                body += "<div class=\"card my-2\">" +
                    "<div class=\"card-body\">" +
                    "<h5 class=\"card-title\">" + listItem[i].TitlePost + "</h5>" +
                    "<p class=\"card-text\">" + listItem[i].BodyPost + "</p>" +
                    "</div></div>"
            }
        }

        body += formElement;
        body += pageFooter;

        res.write(body);
        res.end();
    }
})

server.listen(8081, () => {
    console.log("Server started!");
})
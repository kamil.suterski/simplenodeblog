// Example Headers:
// ================
// 'Host': '127.0.0.1:8080',
// 'Connection': 'keep-alive',
// 'Content-Length': '49',
// 'Cache-Control': 'max-age=0',
// 'Origin': 'http://127.0.0.1:5500',
// 'Upgrade-Insecure - Requests': '1',
// 'Content-Type': 'application/x-www-form-urlencoded',
// 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36(KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36',
// 'Accept': 'text/html,application/xhtml + xml, application/xml;q=0.9,image/webp, image/apng,*/ *; q=0.8',
// 'Referer': 'http://127.0.0.1:5500/index.html',
// 'Accept-Encoding': 'gzip, deflate, br',
// 'Accept-Language': 'pl-PL, pl; q=0.9, en-US; q=0.8, en; q=0.7'

// --------------------------------------------------------------

// const http = require('http'); 
// const url = require('url');

// const server = http.createServer((request, response) => {
	
// 	response.setHeader('Location', 'www.map.smartherd.com/index');
// 	response.setHeader('X-Foo', 'bar');
// 	response.writeHead(200, {'Content-Type':'text/html', 'X':'y'});

// 	const queryData = url.parse(request.url, true).query;
// 	const msg = queryData.name + " is " + queryData.age + " years old";

// 	response.write(msg);
// 	response.end(); 
// }); 

// server.listen(9000, () => { 
// 	console.log('Server is running...'); 
// });

// --------------------------------------------------------------

// const url = require('url');

// const reqUrl = 'http://localhost:9000/user?country=India&city=Delhi';

// const urlObject = url.parse(reqUrl, true);

// console.log(urlObject.host);			// localhost:9000 
// console.log(urlObject.pathname);		//    /user 
// console.log(urlObject.search);		// 	?country=India&city=Delhi 	

// const queryData = urlObject.query			// { country: 'India', city: 'Delhi' } 
// console.log(queryData.country);			// 'India'
// console.log(queryData.city);			// 'Delhi'

// --------------------------------------------------------------

// do dekodowania treści przysłanych wiadomosci POST typu x-www-form-urlencoded służy
// rozszeżenie express.urlencoded.

// At the top of the file
// const { parse } = require('querystring');
// ...
// ...
// if (req.method === 'POST') {
//     let body = '';
//     req.on('data', chunk => {
//         body += chunk.toString();
//     });
//     req.on('end', () => {
//         console.log(
//             parse(body)
//         );
//         res.end('ok');
//     });
// }

const http = require('http');
const url = require('url');
const { parse } = require('querystring');
const fs = require('fs');

var listItem = [];

const server = http.createServer((req, res) => {

    //console.log(req.url);
    
    // https://nodejs.org/docs/latest/api/url.html#url_url_parse_urlstring_parsequerystring_slashesdenotehost
    let currentUrl = url.parse(req.url, true);

    console.log('Host: ' + currentUrl.host);
    console.log('Port: ' + currentUrl.port);
    console.log('Protocol: ' + currentUrl.protocol);
    //console.log('Query'+ currentUrl.query);
    console.log('Search: ' + currentUrl.search);
    console.log('Pathname: ' + currentUrl.pathname);

    res.writeHead(200, {
        'Content-Type': 'text/html'
    });
    res.write("Hello world!");

    if (req.method == 'POST') {
        console.log('Odebrano wiadomość POST');
        //console.log(req);
        let body = [];
        req.on('data', (chunk) => {
            body.push(chunk);
        }).on('end', () => {
            //console.log(Buffer.concat(body).toString())
            console.log(parse(Buffer.concat(body).toString()));
            listItem.push(parse(Buffer.concat(body).toString()));
            //body = Buffer.concat(body).toString();
        })        
    }
})

// podzielić sobie to na sekcje gdzie dla róznych url będą wysyłane wiadomości z inną zawrtością
// w odpowiedzi na wysłanego POST wysyłać komunikat z nagłówkiem Localized: główna strona

// Po odebraniu danych z POST dopisywać do tablicy lub odrazu do pliku odebrane do dane po sparsowaniu
// Dane zapisywac w postaci prostego tekstu i druga wersja w postaci tablicy obiektów jako JSON

// Przy ładowaniu strony z wynikami wykorzystać docelowo partiale i ładować treść z plików

server.listen(8081, () => {
    console.log("Uff udało się uruchomić server")
})